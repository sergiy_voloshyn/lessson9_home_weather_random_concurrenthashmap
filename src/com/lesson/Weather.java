package com.lesson;

import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by user on 16.12.2017.
 * <p>
 * 1. Создать программу "Погода". Погода (ConcurrentHashMap, содержащий название города, температуру, давление, влажность и пр.)
 * обновляется раз в N времени отдельным потоком (значения можно брать случайные). Три других потока проверяют обновления
 * (раз в М время, причем для каждого потока М уникальное) и, если погода изменилась, выводят данные о погоде в консоль
 * на разных языках (для перевода использовать словарь - коллекцию Map, где ключом будет выступать слово на английском языке,
 * а значением - его перевод).
 */

public class Weather extends TimerTask {

    ConcurrentHashMap<String, City> cities;

    @Override
    public void run() {

        //to set
        Set<Map.Entry<String, City>> setCities = cities.entrySet();


        for (Map.Entry<String, City> city : setCities) {

            String cityName = city.getKey();
            /*
             temperature, pressure,humidity,wind;
             */

            City newCity = new City(rnd(-100F, 100F), rnd(0F, 1000F), rnd(0F, 100F), rnd(0F, 200F));

            cities.put(cityName, newCity);
            System.out.println("Generation -" + city.getKey() + " : T=" + city.getValue().temperature +
                    " : H=" + city.getValue().humidity + " : P=" + city.getValue().pressure + " : W=" + city.getValue().wind);
        }
    }

    public Weather(ConcurrentHashMap<String, City> cities) {
        this.cities = cities;
    }

    public static float rnd(float min, float max) {
        max -= min;
        return (float) (Math.random() * ++max) + min;
    }

}
