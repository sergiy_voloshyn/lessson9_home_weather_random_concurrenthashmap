package com.lesson;

import java.util.HashMap;

/**
 * Created by user on 17.12.2017.
 */
public class Translation  {

    //temperature, pressure,humidity,wind;

    public static final String temperature="temperature";
    public static final String pressure="pressure";
    public static final String humidity="humidity";
    public static final String wind="wind";

    HashMap<String, String> citiesNames;

    public Translation(HashMap<String, String> citiesNames) {

        this.citiesNames = citiesNames;
    }
}
