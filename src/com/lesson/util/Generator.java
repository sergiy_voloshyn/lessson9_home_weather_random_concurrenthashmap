package com.lesson.util;

import com.lesson.Translation;

import java.util.HashMap;

public final class Generator {

    private Generator() {
    }

    public static Translation generateRusTranslation() {

        HashMap<String, String> translationRusAdd = new HashMap();
        translationRusAdd.put("Kiev", "Киев");
        translationRusAdd.put("Kharkov", "Харьков");
        translationRusAdd.put("Paris", "Париж");
        //temperature, pressure,humidity,wind;
        translationRusAdd.put("temperature", "температура");
        translationRusAdd.put("pressure", "давление");
        translationRusAdd.put("humidity", "влажность");
        translationRusAdd.put("wind", "ветер");
        Translation translationRus = new Translation(translationRusAdd);
        return translationRus;
    }

    public static Translation generateUkrTranslation() {

        HashMap<String, String> translationUkrAdd = new HashMap();
        translationUkrAdd.put("Kiev", "Киiв");
        translationUkrAdd.put("Kharkov", "Харкiв");
        translationUkrAdd.put("Paris", "Париж");
        translationUkrAdd.put("temperature", "температура");
        translationUkrAdd.put("pressure", "тиск");
        translationUkrAdd.put("humidity", "вологiсть");
        translationUkrAdd.put("wind", "вiтер");
        Translation translationUkr = new Translation(translationUkrAdd);
        return translationUkr;

    }

    public static Translation generateFranceTranslation() {

        HashMap<String, String> translationFranceAdd = new HashMap();
        translationFranceAdd.put("Kiev", "Kiev");
        translationFranceAdd.put("Kharkov", "Kharkiv");
        translationFranceAdd.put("Paris", "Paris");
        translationFranceAdd.put("temperature", "température");
        translationFranceAdd.put("pressure", "la pression");
        translationFranceAdd.put("humidity", "humidité");
        translationFranceAdd.put("wind", "le vent");
        Translation translationFrance = new Translation(translationFranceAdd);
        return translationFrance;
    }
}
