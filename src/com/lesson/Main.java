package com.lesson;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import static com.lesson.util.Generator.generateFranceTranslation;
import static com.lesson.util.Generator.generateRusTranslation;
import static com.lesson.util.Generator.generateUkrTranslation;

/*
1. Создать программу "Погода". Погода (ConcurrentHashMap, содержащий название города, температуру, давление, влажность и пр.)
обновляется раз в N времени отдельным потоком (значения можно брать случайные). Три других потока проверяют обновления
 (раз в М время, причем для каждого потока М уникальное) и, если погода изменилась, выводят данные о погоде в консоль
 на разных языках (для перевода использовать словарь - коллекцию Map, где ключом будет выступать слово на английском языке,
 а значением - его перевод).


 */
public class Main {

    static volatile ConcurrentHashMap<String, City> newCities;

    public static void main(String[] args) {

        newCities = new ConcurrentHashMap<String, City>();
        City city = new City();

        newCities.put("Kiev", city);
        newCities.put("Kharkov", city);
        newCities.put("Paris", city);


        Translation translationRus = generateRusTranslation();
        Translation translationUkr = generateUkrTranslation();
        Translation translationFrance = generateFranceTranslation();

        Timer weatherTimer = new Timer();
        TimerTask weatherTask = new Weather(newCities);
        weatherTimer.schedule(weatherTask, 0, 5000);

        Timer checkWeatherTimer1 = new Timer();
        TimerTask checkWeatherTask1 = new CheckWeather("Kiev", "thread 1", translationRus);
        checkWeatherTimer1.schedule(checkWeatherTask1, 1000, 2000);

        Timer checkWeatherTimer2 = new Timer();
        TimerTask checkWeatherTask2 = new CheckWeather("Kharkov", "thread 2", translationUkr);
        checkWeatherTimer2.schedule(checkWeatherTask2, 1000, 3000);

        Timer checkWeatherTimer3 = new Timer();
        TimerTask checkWeatherTask3 = new CheckWeather("Paris", "thread 3", translationFrance);
        checkWeatherTimer3.schedule(checkWeatherTask3, 1000, 4000);


    }
}

