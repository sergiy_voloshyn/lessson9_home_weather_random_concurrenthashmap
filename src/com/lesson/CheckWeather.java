package com.lesson;


import java.util.TimerTask;


import static com.lesson.Main.newCities;
import static com.lesson.Translation.*;

/**
 * Created by user on 17.12.2017.
 */
public class CheckWeather extends TimerTask {


    String cityName;
    String threadName;
    Translation translation;

    volatile City oldCity = new City();

    @Override
    public void run() {


        if (newCities.containsKey(cityName)) {

            City tempCity = newCities.get(cityName);
            if (!oldCity.equals(tempCity)) {

                System.out.println(threadName + " : " + translation.citiesNames.get(cityName) + "  " + translation.citiesNames.get(temperature) +
                        " : = " + tempCity.temperature + " " + translation.citiesNames.get(humidity) + " : = " + tempCity.humidity + " " +
                        translation.citiesNames.get(pressure) + " : = " + tempCity.pressure + " " + translation.citiesNames.get(wind) + " : = " + tempCity.wind);
                oldCity = tempCity;
            }
        }
    }


    public CheckWeather(String cityName, String threadName, Translation translation) {
        this.cityName = cityName;
        this.threadName = threadName;
        this.translation = translation;
    }
}
