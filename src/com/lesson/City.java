package com.lesson;

import static com.lesson.Weather.rnd;

/**
 * Created by user on 16.12.2017.
 */

/*
1. Создать программу "Погода". Погода (ConcurrentHashMap, содержащий название города, температуру, давление, влажность и пр.)
обновляется раз в N времени отдельным потоком (значения можно брать случайные). Три других потока проверяют обновления
 (раз в М время, причем для каждого потока М уникальное) и, если погода изменилась, выводят данные о погоде в консоль
 на разных языках (для перевода использовать словарь - коллекцию Map, где ключом будет выступать слово на английском языке,
 а значением - его перевод).


 */

public class City {

    float temperature;
    float pressure;
    float humidity;
    float wind;

    public City(float temperature, float pressure, float humidity, float wind) {

        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind = wind;
    }
  public City() {
// City(rnd(-100F, 100F), rnd(0F, 1000F), rnd(0F, 100F), rnd(0F, 200F));
        this.temperature =rnd(-100F, 100F) ;
        this.pressure = rnd(0F, 1000F);
        this.humidity = rnd(0F, 100F);
        this.wind = rnd(0F, 200F);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;

        City city = (City) o;

        if (Float.compare(city.temperature, temperature) != 0) return false;
        if (Float.compare(city.pressure, pressure) != 0) return false;
        if (Float.compare(city.humidity, humidity) != 0) return false;
        return Float.compare(city.wind, wind) == 0;
    }

    @Override
    public int hashCode() {
        int result = (temperature != +0.0f ? Float.floatToIntBits(temperature) : 0);
        result = 31 * result + (pressure != +0.0f ? Float.floatToIntBits(pressure) : 0);
        result = 31 * result + (humidity != +0.0f ? Float.floatToIntBits(humidity) : 0);
        result = 31 * result + (wind != +0.0f ? Float.floatToIntBits(wind) : 0);
        return result;
    }
}
